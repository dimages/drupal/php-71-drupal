# from https://www.drupal.org/requirements/php#drupalversions
FROM php:7.1-fpm

WORKDIR /root
ENV TERM=xterm

# install the PHP extensions we need
RUN set -ex; \
	\
	if command -v a2enmod; then \
		a2enmod rewrite; \
	fi; \
	\
	savedAptMark="$(apt-mark showmanual)"; \
	\
	apt-get update; \
	apt-get install -y --no-install-recommends \
	    build-essential \
		libjpeg-dev \
		libpng-dev \
		libpq-dev \
	; \
	\
	docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr; \
	docker-php-ext-install -j "$(nproc)" \
		gd \
		opcache \
		pdo_mysql \
		pdo_pgsql \
		zip \
		bcmath \
	; \
	\
# reset apt-mark's "manual" list so that "purge --auto-remove" will remove all build dependencies
	apt-mark auto '.*' > /dev/null; \
	apt-mark manual $savedAptMark; \
	ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
		| awk '/=>/ { print $3 }' \
		| sort -u \
		| xargs -r dpkg-query -S \
		| cut -d: -f1 \
		| sort -u \
		| xargs -rt apt-mark manual; \
	\
	apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
	rm -rf /var/lib/apt/lists/*

# set recommended PHP.ini settings
# see https://secure.php.net/manual/en/opcache.installation.php
RUN { \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=60'; \
		echo 'opcache.fast_shutdown=1'; \
		echo 'opcache.enable_cli=1'; \
	} > /usr/local/etc/php/conf.d/opcache-recommended.ini

# copy configurations
# COPY memory.ini /usr/local/etc/php/conf.d/memory.ini
# COPY www.conf /usr/local/etc/php-fpm.d/www.conf

# install composer and mysql-client
# https://getcomposer.org/download/
RUN apt-get update && \
    apt-get install -y curl mysql-client && \
    curl -o /tmp/composer-setup.php https://getcomposer.org/installer && \
    curl -o /tmp/composer-setup.sig https://composer.github.io/installer.sig && \
    # Make sure we're installing what we think we're installing!
    php -r "if (hash('SHA384', file_get_contents('/tmp/composer-setup.php')) !== trim(file_get_contents('/tmp/composer-setup.sig'))) { unlink('/tmp/composer-setup.php'); echo 'Invalid installer' . PHP_EOL; exit(1); }" && \
    php /tmp/composer-setup.php --no-ansi --install-dir=/usr/local/bin --filename=composer --snapshot && \
    rm -f /tmp/composer-setup.*

# MSMTP
RUN apt-get update && \
    apt-get install -y automake gcc gettext texinfo ca-certificates libgnutls28-dev libgsasl7-dev libidn11-dev libsecret-1-dev git wget unzip nano && \
    git clone git://git.code.sf.net/p/msmtp/code msmtp && \
#    git clone https://git.code.sf.net/p/msmtp/code msmtp && \
    cd msmtp && \
    autoreconf -i && \
    ./configure --prefix=/usr/local && \
    make && \
    make install && \
    touch /var/log/msmtp.log && \
    chmod +x /var/log/msmtp.log && \
    chown -R www-data:adm /var/log/msmtp.log && \
    cd .. && \
    rm -R msmtp && \
    apt-get remove -y automake gcc gettext texinfo && \
    apt-get autoremove -y && \
    apt-get clean

WORKDIR /var/www/html
CMD ["php-fpm"]